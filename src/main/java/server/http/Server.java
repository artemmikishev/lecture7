package server.http;

import com.sun.net.httpserver.HttpServer;
import org.example.db.PersonDao;
import server.handlers.PersonHandler;
import server.handlers.WelcomeHandler;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

public class Server {
    private final String IP;
    private final int PORT;

    /**
     * Конструктор по умолчанию.
     */
    public Server(){
        this.IP = "localhost";
        this.PORT = 8000;
    }

    /**
     * Конструктор с параметрами.
     * @param IP - ip адрес сервера.
     * @param PORT - port сервера.
     */
    public Server(String IP, int PORT){
        this.IP = IP;
        this.PORT = PORT;
    }


    /**
     * Метод для запуска сервера
     */
    public void startServer() {
        try {
            HttpServer server = HttpServer.create(new InetSocketAddress(IP, PORT), 0);
            server.setExecutor(Executors.newFixedThreadPool(10));
            server.createContext("/welcome", new WelcomeHandler());
            server.createContext("/persons", new PersonHandler(new PersonDao()));
            server.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

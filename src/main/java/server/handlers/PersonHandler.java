package server.handlers;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.example.Person;
import org.example.db.PersonDao;
import org.json.JSONObject;
import org.testng.annotations.IFactoryAnnotation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class PersonHandler implements HttpHandler
{

    private final PersonDao dao;

    public PersonHandler(PersonDao dao) {
        this.dao = dao;
    }

    @Override
    public void handle(HttpExchange exchange)  {
        OutputStream outputStream = exchange.getResponseBody();

        String valueOfDao = "empty";
        if (exchange.getRequestMethod().equalsIgnoreCase("GET"))
        {
            valueOfDao = this.getExecute();
        }
        else if (exchange.getRequestMethod().equalsIgnoreCase("POST"))
        {
            valueOfDao = this.postExecute(exchange);
        }
        else if (exchange.getRequestMethod().equalsIgnoreCase("DELETE"))
        {
            valueOfDao = this.deleteExecute(exchange);
        }
        else if (exchange.getRequestMethod().equalsIgnoreCase("PUT"))
        {
            valueOfDao = this.changeExecute(exchange);
        }
        StringBuilder htmlBuilder = new StringBuilder();
        htmlBuilder.append("<html")
                .append("<body>")
                .append("<h1>")
                .append("<center>")
                .append("DAO result")
                .append("</center>")
                .append(valueOfDao)
                .append("</h1")
                .append("</body>")
                .append("</html>");

        String htmlResponse = htmlBuilder.toString();
        try {
            exchange.sendResponseHeaders(200, htmlResponse.length());
            outputStream.write(htmlResponse.getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                outputStream.flush();
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }




    }

    private String findAllForHtml() {
        StringBuilder result = new StringBuilder();
        List<Person> personList = dao.findAll();
        Iterator<Person> personIterator = personList.iterator();
        result.append("<h3>");
        result.append("<ul>");
        while (personIterator.hasNext()) {
            result.append("<br>");
            result.append("<li>");
            result.append(personIterator.next());
            result.append("</li>");
        }
        result.append("</ul>");
        result.append("</h3>");

        return result.toString();
    }

    private String getExecute()
    {
        return this.findAllForHtml();
    }

    private String postExecute(HttpExchange exchange)
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(exchange.getRequestBody()));

        String requestBody = reader.lines().collect(Collectors.joining());
        JSONObject jsonObject = new JSONObject(requestBody);
        int id = jsonObject.getInt("id");
        String name = jsonObject.getString("name");
        int age = jsonObject.getInt("age");


        Person newPerson = new Person(id, name, age);
        this.dao.addPerson(newPerson);
        return "true";
    }

    /**
     * Метод для удаления элемента из таблицы.
     * @param exchange
     * @return
     */
    private String deleteExecute(HttpExchange exchange) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(exchange.getRequestBody()));

        String requestBody = reader.lines().collect(Collectors.joining());

        JSONObject jsonObject = new JSONObject(requestBody);

        if (jsonObject.toString().contains("id"))
        {
            int id = jsonObject.getInt("id");
            dao.deleteById(id);
        } else
            return "false";
        return "true";
    }

    /**
     * Метод для изменения элемента в таблице по id
     * @param exchange
     * @return true, если элемент успешно изменен
     * false - если нет
     */
    private String changeExecute(HttpExchange exchange) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(exchange.getRequestBody()));

        String requestBody = reader.lines().collect(Collectors.joining());
        JSONObject jsonObject = new JSONObject(requestBody);

        if (jsonObject.toString().contains("id") && (jsonObject.toString().contains("name")))
        {
            int id = jsonObject.getInt("id");
            String name = jsonObject.getString("name");
            dao.changeNameById(id, name);
        } else
            return "false";
        return "true";
    }

}

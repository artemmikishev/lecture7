package org.example.db;

import org.example.Person;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class PersonDao {

    final String localURL = "jdbc:sqlite:C:\\sqlite\\db\\chinook.db";


    /**
     * Создаем таблицу.
     * @throws SQLException
     */
    public void createTable() {

        String sql = "create table Persons (" +
                " id varchar(32)," +
                " name varchar(32)," +
                " age varchar(32)" +
                ");";

        try (Connection connection = DriverManager.getConnection(localURL);
             Statement statement = connection.createStatement())
        {
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }
    /**
     * Добавляем продукт в таблицу.
     * @param person
     * @throws SQLException
     */
    public void addPerson(Person person) {
        final String createPersonRequest = String.format("insert into Persons ('id', 'name', 'age') VALUES ('%s', '%s', '%s')",
                person.getId(), person.getName(), person.getAge());

        try (Connection connection = DriverManager.getConnection(localURL);
             Statement statement = connection.createStatement())
        {
            statement.executeUpdate(createPersonRequest);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Изменяем имя в таблице, по исходному имени.
     * @param sourceName - Исходное имя
     * @param newName - Новое имя
     */

    public void changeNameBySrcName(String sourceName, String newName){

        final String changePersonRequest = "update Persons set name = '" + newName + "' where name ='" + sourceName + "';";

        try (Connection connection = DriverManager.getConnection(localURL);
             Statement statement = connection.createStatement()) {

            statement.executeUpdate(changePersonRequest);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    /**
     * Изменяем имя в таблице, по id.
     * @param id - id пользователя в таблице
     * @param newName - Новое имя
     */

    public void changeNameById(int id, String newName){

        final String changePersonRequest = "update Persons set name = '" + newName + "' where id ='" + id + "';";

        try (Connection connection = DriverManager.getConnection(localURL);
             Statement statement = connection.createStatement()) {

            statement.executeUpdate(changePersonRequest);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }



    /**
     * Удаляем таблицу.
     * @throws SQLException
     */
    public void dropTable() {
        final String dropTableRequest = "drop table Persons";
        try (Connection connection = DriverManager.getConnection(localURL);
             Statement statement = connection.createStatement()) {

            statement.executeUpdate(dropTableRequest);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * Находим все элементы таблицы и создаем объект List<Person>.
     * @return
     * @throws SQLException
     */
    public List<Person> findAll(){

        try (Connection connection = DriverManager.getConnection(localURL);
             Statement statement = connection.createStatement()) {

            String sql = "select * from Persons";

            List<Person> products = new ArrayList<>();
            try (ResultSet resultSet = statement.executeQuery(sql)) {


                while (resultSet.next()) {
                    int id = Integer.parseInt(resultSet.getString("id"));
                    String name = resultSet.getString("name");
                    int age = Integer.parseInt(resultSet.getString("age"));
                    Person person = new Person(id, name, age);
                    products.add(person);
                }
                return products;
            }
        } catch (Exception ex) {
            throw new RuntimeException("Failed to .findAll error = " + ex.toString(), ex);
        }
    }

    /**
     * Находим person по id
     * @param searchId
     * @return
     * @throws SQLException
     */
    public Person findById(int searchId) {
        int id = 0;
        String name = "nobody";
        int age = 0;
        try(Connection connection = DriverManager.getConnection(localURL);
            Statement statement = connection.createStatement()) {

            statement.execute("select * from Persons where id = '" + searchId + "'");
            ResultSet resultSet = statement.getResultSet();

            if (!resultSet.next()) {
                return new Person(0, "nobody", 0);
            }
            id = Integer.parseInt(resultSet.getString("id"));
            name = resultSet.getString("name");
            age = Integer.parseInt(resultSet.getString("age"));
        } catch (Exception ex) {
            throw new RuntimeException("Failed to .findAll error = " + ex.toString(), ex);
        }
        return new Person(id, name, age);
    }

    /**
     * Удаление элемента из таблицы по id.
     * @param id - id person
     * @throws SQLException
     */
    public void deleteById(int id) {

        try (Connection connection = DriverManager.getConnection(localURL);
             Statement statement = connection.createStatement()) {

            statement.executeUpdate("delete from Persons where id = " + id);



        } catch (Exception ex) {
            throw new RuntimeException("Failed to deleteById " + id + " error = " + ex.toString(), ex);
        }


    }
}

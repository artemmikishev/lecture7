package io;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.Date;

public class MyIOExample
{
    /**
     * Метод проверяет,
     * если сущность существует, то выводит в консоль информацию:
     *      - абсолютный путь
     *      - родительский путь.
     * Если сущность является файлом, то выводит в консоль:
     *      - размер
     *      - время последнего изменения
     * @param fileName - имя файла
     * true, если файл существует
     */
    public boolean workWithFile(String fileName) {
        Path path = Paths.get(fileName);
        boolean exists = Files.exists(path);
        if (exists) {
            System.out.println("Is absolute? " + path.isAbsolute());
            Path currentParent = path;
            while ((currentParent = currentParent.getParent()) != null) {
                System.out.println(" Current parent is: " + currentParent);
            }
            boolean isFile = Files.isRegularFile(path);
            if (isFile){
                try {
                    System.out.println("File size = " + Files.size(path));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    System.out.println("Last modified time " + Files.getLastModifiedTime(path));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return true;
            }
        }
        return false;
    }

    /**
     * Метод создает копию файла, используя, встроенный метод, Files.copy.
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * true, если файл успешно скопирован
     */
    public boolean copyFile(String sourceFileName, String destinationFileName)
    {
        try {
            Files.copy(Paths.get(sourceFileName), Paths.get(destinationFileName));
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Метод создает копию файла, используя, BufferedReader и BufferedWriter
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * true, если файл успешно скопирован
     */
    public boolean copyBufferedFile(String sourceFileName, String destinationFileName)
    {
        Path sourcePath = Paths.get(sourceFileName);
        Path destinationPath = Paths.get(destinationFileName);
        try (BufferedReader reader = Files.newBufferedReader(sourcePath);
             BufferedWriter writer = Files.newBufferedWriter(destinationPath)) {

            while (reader.ready()) {
                writer.write(reader.read());
            }
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Метод создает копию файла, используя, FileReader и FileWriter
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * true, если файл успешно скопирован
     */
    public boolean copyFileWithReaderAndWriter(String sourceFileName, String destinationFileName)
    {
        Path sourcePath = Paths.get(sourceFileName);
        Path destinationPath = Paths.get(destinationFileName);
        try (FileReader reader = new FileReader(sourcePath.toFile());
             FileWriter writer = new FileWriter(destinationPath.toFile())) {

            while (reader.ready()) {
                writer.write(reader.read());
            }
            return true;

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return false;
    }
}

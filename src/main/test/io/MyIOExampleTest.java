package io;

import db.MyIOExample;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


public class MyIOExampleTest {

    MyIOExample myIOExample = new MyIOExample();

    /**
     * Тест метода workWithFile().
     * Если файл существует, то вывод должен быть true.
     */
    @Test
    public void workWithFilePositive() {
        Assert.assertTrue(myIOExample.workWithFile("MyFile"));
        Assert.assertFalse(myIOExample.workWithFile("NotFile"));
    }

    /**
     * Тест метода copyFile(). С помощью встроенного метода Files.copy().
     * Если файл копировался и создался, то вывод должен быть true.
     */
    @Test
    public void copyFile() {
        Assert.assertTrue(myIOExample.copyFile("MyFile", "NewMyFileCopy"));
        Path destFile = Paths.get("NewMyFileCopy");
        Assert.assertTrue(Files.exists(destFile));
    }

    /**
     * Тест метода copyFile(). С помощью BufferedReader и BufferedWriter.
     * Если файл копировался и создался, то вывод должен быть true.
     */
    @Test
    public void copyBufferedFile() {
        Assert.assertTrue(myIOExample.copyFile("MyFile", "NewMyFileBuffer"));
        Path destFile = Paths.get("NewMyFileBuffer");
        Assert.assertTrue(Files.exists(destFile));
    }

    /**
     * Тест метода copyFile(). С помощью FileReader и FileWriter.
     * Если файл копировался и создался, то вывод должен быть true.
     */
    @Test
    public void copyFileWithReaderAndWriter() {
        Assert.assertTrue(myIOExample.copyFile("MyFile", "NewMyFileFileWriter"));
        Path destFile = Paths.get("NewMyFileFileWriter");
        Assert.assertTrue(Files.exists(destFile));
    }
}
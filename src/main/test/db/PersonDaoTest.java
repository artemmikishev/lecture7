package db;

import org.example.Person;
import org.example.db.PersonDao;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class PersonDaoTest {

    PersonDao dao = new PersonDao();



    /**
     * Тест метода по созданию таблицы.
     */
    @Test
    public void createTableTest() {
        dao.createTable();
    }

    /**
     * Тест метода по добавлению Person в таблицу,
     * добавляем по порядку от 1 до 5 и случайным возрастом.
     */
    @Test
    public void addPersonTest() {
        for (int i = 1; i <= 5; i++) {
            int age = (int) (Math.random() * 100);
            Person person = new Person(i, "Person_" + i, age);
            dao.addPerson(person);
        }

    }

    /**
     * Тест метода по поиску person в таблице по id.
     * Позитивный сценарий.
     *
     */
    @Test
    public void findByIdTestPositive() {
        Person person = new Person(50, "Andrey", 50);
        dao.addPerson(person);
        Person findPerson = dao.findById(50);
        Assert.assertEquals(findPerson, person);
    }

    /**
     * Тест метода по поиску person в таблице по id.
     * Негативный сценарий. Должен вернуть нам person(0, "nobody", 0);
     */
    @Test
    public void findByIdTestNegative() {
        Person findPerson = dao.findById(49);
        Person zeroPerson = new Person(0, "nobody", 0);
        Assert.assertEquals(findPerson, zeroPerson);
    }

    /**
     * Тест метода, по изменению имени в таблице по исходному имени.
     */
    @Test
    public void changeNameBySrcNameTest() {
        Person addPerson = new Person(105, "Pert", 50);
        dao.addPerson(addPerson);
        dao.changeNameBySrcName("Pert", "Petr");
        Person correctPerson = new Person(105, "Petr", 50);
        Assert.assertEquals(correctPerson, dao.findById(105));
    }

    /**
     * Тест метода, по изменению имени в таблице по id.
     */
    @Test
    public void changeNameByIdTest() {
        Person addPerson = new Person(74, "Pert", 50);
        dao.addPerson(addPerson);
        dao.changeNameById(74, "Petr");
        Person correctPerson = new Person(74, "Petr", 50);
        Assert.assertEquals(correctPerson, dao.findById(74));
    }


    /**
     * Тест метода по выводу всех элементов из таблицы.
     */
    @Test
    public void findAll() {
        List<Person> personList = new ArrayList<>();
        for (int i = 1; i <= 5; i++) {
            int age = (int) (Math.random() * 100);
            Person person = new Person(i, "Person_" + i, age);
            dao.addPerson(person);
            personList.add(person);
        }
        List<Person> findList = dao.findAll();
        Assert.assertTrue(findList.containsAll(personList));

    }

    /**
     * Удаление элемента из таблицы по id.
     */

    @Test
    public void deleteById() {
        dao.addPerson(new Person(33, "Somebody", 33));
        dao.deleteById(33);
        Person findPerson = dao.findById(33);
        Person zeroPerson = new Person(0, "nobody", 0);
        Assert.assertEquals(findPerson, zeroPerson);

    }

    /**
     * Удаление таблицы.
     */
    @Test
    public void dropTable() {
        dao.dropTable();
    }
}